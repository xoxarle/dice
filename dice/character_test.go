package dice

import (
	"fmt"
	"testing"
)

func TestCreateCharacter(t *testing.T) {
	notIn := func(imin, imax, ival int) bool {
		return !(imin <= ival && ival <= imax)
	}

	t.Run("Character must be fully formed", func(t *testing.T) {
		persona := CreateCharacter()
		if notIn(3, 18, persona.Str) || notIn(3, 18, persona.Int) || notIn(3, 18, persona.Dex) || notIn(3, 18, persona.Wis) || notIn(3, 18, persona.Cos) || notIn(3, 18, persona.Cha) {
			t.Errorf("Stats must be between 3 and 18")
		} else {
			fmt.Printf("%v\n", persona)
		}

	})
}

func TestRollStat(t *testing.T) {
	t.Run("Stats must be from 3 to 18", func(t *testing.T) {
		roll := RollStat()
		if roll < 3 || roll > 18 {
			t.Errorf("RollStat yielded %d should be between 3 and 18", roll)
		}

	})
}
