package dice

import (
	"fmt"
	"math/rand"
	"strings"
	"testing"
	"time"
)

func TestD4(t *testing.T) {
	rand.Seed(time.Now().UTC().UnixNano())
	VALUE := 4
	for h := 1; h < 20; h++ {
		roll := D4()
		expected := (roll >= 1 && roll <= VALUE)
		if !expected {
			t.Errorf("Should return a number between 1 and %d, got %d instead", VALUE, roll)
		}
	}

}

func TestD6(t *testing.T) {
	roll := D6()
	expected := (roll >= 1 && roll <= 6)
	if !expected {
		t.Errorf("Should return a number between 1 and 6")
	}

}

func TestD8(t *testing.T) {
	VALUE := 8
	roll := D8()
	expected := (roll >= 1 && roll <= VALUE)
	if !expected {
		t.Errorf("Should return a number between 1 and %d, got %d instead", VALUE, roll)
	}

}

func TestD10(t *testing.T) {
	VALUE := 10
	roll := D10()
	expected := (roll >= 1 && roll <= VALUE)
	if !expected {
		t.Errorf("Should return a number between 1 and %d, got %d instead", VALUE, roll)
	}

}

func TestD12(t *testing.T) {
	VALUE := 12
	roll := D12()
	expected := (roll >= 1 && roll <= VALUE)
	if !expected {
		t.Errorf("Should return a number between 1 and %d, got %d instead", VALUE, roll)
	}

}

func TestD20(t *testing.T) {
	VALUE := 20
	roll := D20()
	expected := (roll >= 1 && roll <= VALUE)
	if !expected {
		t.Errorf("Should return a number between 1 and %d, got %d instead", VALUE, roll)
	}

}

func TestRoll(t *testing.T) {
	t.Run("Must return between 3 and 18", func(t *testing.T) {
		roll := -1
		for i := 1; i < 100; i++ {
			roll = Roll(3, 6) //3d6
			if roll < 3 || roll > 18 {
				t.Errorf("Roll(3,6) should mean \"Roll 3d6\", thus yielding a value between 3 and 18. got %d instead", roll)
			}
		}
	})
	t.Run("Must return -1 when calling Roll(5,0)", func(t *testing.T) {
		roll := Roll(5, 0)
		if roll != -1 {
			t.Errorf("Roll(5,0) should return -1. got %d instead", roll)
		}
	})
	t.Run("Must return -1 when calling Roll(0,5)", func(t *testing.T) {
		roll := Roll(0, 5)
		if roll != -1 {
			t.Errorf("Roll(5,6) should return -1. got %d instead", roll)
		}
	})
}

func TestRollKeepWorst(t *testing.T) {
	t.Run("Average RollKeepWorst(6,4,3) should be < 10.0", func(t *testing.T) {
		r := 0
		for i := 0; i < 100000; i++ {
			r += RollKeepWorst(6, 4, 3)
		}
		avg := float64(r) / 100000.0
		fmt.Println(avg)

		if avg > 10.0 {
			t.Errorf("RollKeepBest(6,4,0) should average <10.0 returns %f", avg)
		}
	})
}

func TestRollKeepBest(t *testing.T) {
	rand.Seed(time.Now().UTC().UnixNano())

	t.Run("RollKeepBest(6,4,0) should return -1", func(t *testing.T) {
		roll := RollKeepBest(6, 4, 0)
		if roll != -1 {
			t.Errorf("RollKeepBest(6,4,0) should return -1 returns %d", roll)
		}
	})
	t.Run("RollKeepBest(6,0,0) should return -1", func(t *testing.T) {
		roll := RollKeepBest(6, 0, 0)
		if roll != -1 {
			t.Errorf("RollKeepBest(6,0,0) should return -1 returns %d", roll)
		}

	})
	t.Run("RollKeepBest(0,4,0) should return -1", func(t *testing.T) {
		roll := RollKeepBest(0, 4, 0)
		if roll != -1 {
			t.Errorf("RollKeepBest(0,4,0) should return -1 returns %d", roll)
		}

	})
	t.Run("RollKeepBest(6,4,5) should return -1: keep must be <= die to roll", func(t *testing.T) {
		roll := RollKeepBest(6, 4, 5)
		if roll != -1 {
			t.Errorf("RollKeepBest(6,4,0) should return -1 returns %d", roll)
		}
	})

	t.Run("RollKeepBest(6,4,3) should return > 2:  must be <= die to roll", func(t *testing.T) {
		roll := RollKeepBest(6, 4, 3)
		if roll <= 2 {
			t.Errorf("RollKeepBest(6,4,3) should return >=3 returns %d", roll)
		}
	})

	t.Run("RollKeepBest(6,4,3) should return <= 18: keep must be <= die to roll", func(t *testing.T) {
		roll := RollKeepBest(6, 4, 3)
		if roll > 18 {
			t.Errorf("RollKeepBest(6,4,0) should return <=18 returns %d", roll)
		}
	})

	t.Run("Average RollKeepBest(6,4,3) should be > 11.0", func(t *testing.T) {
		r := 0
		for i := 0; i < 100000; i++ {
			r += RollKeepBest(6, 4, 3)
		}
		avg := float64(r) / 100000.0
		fmt.Println(avg)

		if avg < 11.0 {
			t.Errorf("RollKeepBest(6,4,0) should average >10.5 returns %f", avg)
		}
	})
}

func TestDn(t *testing.T) {
	t.Run("Must return -1 with less < 1 parameter", func(t *testing.T) {
		s := Dn(0)
		t.Helper()
		if s != -1 {
			t.Errorf("Calling Dn with parameters <= 0 should return -1")
		}
	})

	t.Run("Must return a value <= than parameter", func(t *testing.T) {
		for h := 0; h < 10; h++ {
			s := Dn(6)
			if s < 1 || s > 6 {
				t.Errorf("Calling Dn with 6  should return 1,2,3,4,5,6 instead is %d", s)
			}
		}
	})
}
func TestVowel(t *testing.T) {
	t.Run("Must return a single vowel", func(t *testing.T) {
		chars := "aeiouy"
		vow := Vowel()
		if len([]rune(vow)) != 1 || !strings.Contains(chars, vow) {
			t.Errorf("Waiting for a vowel: a,e,i,o,u. Instead, got %s", vow)
		}
	})
}

func TestConsonant(t *testing.T) {
	t.Run("Must return a single consonant", func(t *testing.T) {
		chars := "bcdfghjklmnprstvwxz"
		cons := Consonant()
		if len([]rune(cons)) != 1 || !strings.Contains(chars, cons) {
			t.Errorf("Waiting for a consonant, Instead, got %s", cons)
		}
	})
}

func TestWord(t *testing.T) {
	t.Run("Must return a 6-letter word without spaces", func(t *testing.T) {
		w := Word(6)
		z := len([]rune(w))
		if z != 6 || strings.Contains(w, " ") {
			t.Errorf("Word's length != 6 runes, or it contains spaces. got %s, length: %d", w, z)
		}
	})
}
