package dice

import (
	"math/rand"
	"sort"
	"time"
)

func D4() int  { return Dn(4) }
func D6() int  { return Dn(6) }
func D8() int  { return Dn(8) }
func D10() int { return Dn(10) }
func D12() int { return Dn(12) }
func D20() int { return Dn(20) }

func Dn(die int) int {
	if die <= 0 {
		return -1
	} else {
		return 1 + rand.Int()%die
	}
}

func Roll(die, number int) int {
	if die <= 0 || number <= 0 {
		return -1
	} else {
		res := 0
		for i := 0; i < number; i++ {
			res += Dn(die)
		}
		return res
	}
}

func checkRollKeepParameters(die, number, keep int) int {
	if (die <= 0 || number <= 0) || keep <= 0 {
		return -1
	}
	if keep > number {
		return -1
	}
	return 0
}

func RollKeepBest(die int, number int, keep int) int {
	return rollKeep(die, number, keep, true)
}

func RollKeepWorst(die int, number int, keep int) int {
	return rollKeep(die, number, keep, false)
}

func rollKeep(die int, number int, keep int, best bool) int {
	check := checkRollKeepParameters(die, number, keep)
	if check < 0 {
		return check
	}
	arr := make([]int, number)
	for i := 0; i < number; i++ {
		arr[i] = Dn(die)
	}
	if best {
		sort.Sort(sort.Reverse(sort.IntSlice(arr)))

	} else {
		sort.Sort(sort.IntSlice(arr))
	}
	res := 0
	for i := 0; i < keep; i++ {
		res += arr[i]
	}
	return res
}

const VOWELS = "aeiouy"
const CONSONANT = "bcdfghjklmnprstvwxz"

func Vowel() string {
	l := len([]rune(VOWELS))
	idx := rand.Int() % l
	return string([]rune(VOWELS)[idx : idx+1])
}

func Consonant() string {
	l := len([]rune(CONSONANT))
	idx := rand.Int() % l
	return string([]rune(CONSONANT)[idx : idx+1])
}

func Word(ln int) string {
	odd := rand.Int()%2 == 0
	res := ""
	for h := 0; h < ln; h++ {
		if h%2 == 0 {
			if odd {
				res += Vowel()
			} else {
				res += Consonant()
			}
		} else {
			if odd {
				res += Consonant()
			} else {
				res += Vowel()
			}
		}
		if D6() == 3 {
			odd = !odd
		}
	}
	return res
}

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}
