package dice

type Character struct {
	Name string
	Str  int
	Int  int
	Dex  int
	Wis  int
	Cos  int
	Cha  int
	HP   int
}

// RollStat roll a 4d6-keep best 3 stat
func RollStat() int {
	return RollKeepBest(6, 4, 3)
}

func CreateCharacter() *Character {
	persona := &Character{
		Name: Word(3 + D6()),
		Str:  RollStat(),
		Int:  RollStat(),
		Dex:  RollStat(),
		Wis:  RollStat(),
		Cos:  RollStat(),
		Cha:  RollStat(),
		HP:   Dn(8),
	}
	return persona
}
